from qgis.core import *
from qgis.gui import *
import re

@qgsfunction(args='auto', group='Custom', referenced_columns=[])
def getformattedstreetname(value1, feature, parent):
    parts = value1.split()
    parts = map(formatstreetname, parts)
    return " ".join(parts)

@qgsfunction(args='auto', group='Custom', referenced_columns=[])
def getformattedstreetnamefromaddress(value1, feature, parent):
    parts = value1.split()
    parts.pop(0) # Ignore the first bit (i.e. "123" in "123 N MAIN ST")
    parts = map(formatstreetname, parts)
    return " ".join(parts)

def formatstreetname(name):
    # Specific suffixes like "123th" we have lower
    if re.search("[0-9]+TH", name):
        return name.capitalize()
    if re.search("[0-9]+ND", name):
        return name.capitalize()
    if re.search("[0-9]+ST", name):
        return name.capitalize()
    if re.search("[0-9]+RD", name):
        return name.capitalize()
    # Weird names like 123D we keep upper
    if re.search("[0-9]+[A-Z]+", name):
        return name
    # Highway/etc prefixes
    if name == "US":
        return "US"
    if name == "SR":
        return "SR"
    if name == "CR":
        return "County Road"
    if name == "C":
        return "C"
    # Directions
    if name == "N":
        return "North"
    if name == "NE":
        return "Northeast"
    if name == "E":
        return "East"
    if name == "SE":
        return "Southeast"
    if name == "S":
        return "South"
    if name == "SW":
        return "Southwest"
    if name == "W":
        return "West"
    if name == "NW":
        return "Northwest"
    # Suffixes
    if name == "AVE":
        return "Avenue"
    if name == "BLVD":
        return "Boulevard"
    if name == "BND":
        return "Bend"
    if name == "CIR":
        return "Circle"
    if name == "CT":
        return "Court"
    if name == "DR":
        return "Drive"
    if name == "FLDS":
        return "Fields"
    if name == "GRV":
        return "Grove"
    if name == "HOLW":
        return "Hollow"
    if name == "HWY":
        return "Highway"
    if name == "LN":
        return "Lane"
    if name == "LOOP":
        return "Loop"
    if name == "PATH":
        return "Path"
    if name == "PL":
        return "Place"
    if name == "RD":
        return "Road"
    if name == "RDG":
        return "Ridge"
    if name == "RUN":
        return "Run"
    if name == "ST":
        return "Street"
    if name == "TER":
        return "Terrace"
    if name == "TRL":
        return "Trail"
    if name == "VW":
        return "View"
    if name == "WAY":
        return "Way"
    if name == "XING":
        return "Crossing"
    # Irish names
    if name == "MCCRAY":
        return "McCray"
    if name == "MCKOWN":
        return "McKown"
    return name.capitalize()
